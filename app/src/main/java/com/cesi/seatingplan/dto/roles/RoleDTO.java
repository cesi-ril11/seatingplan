package com.cesi.seatingplan.dto.roles;

public class RoleDTO {
    private Long id;
    private String libelle;

    public Long getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
