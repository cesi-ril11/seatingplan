package com.cesi.seatingplan.dto.bureaux;

public class BureauDTO {
    private Long id;
    private String num_bureau;

    public Long getId() {
        return id;
    }

    public String getNumBureau() {
        return num_bureau;
    }

    /*public boolean isPrivate() {
        return privatif;
    }*/

    /*public void setPrivate(boolean prive) {
        this.privatif = prive;
    }*/
}
