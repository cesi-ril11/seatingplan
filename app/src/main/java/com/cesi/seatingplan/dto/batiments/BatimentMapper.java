package com.cesi.seatingplan.dto.batiments;

import com.cesi.seatingplan.dto.IMapper;
import com.cesi.seatingplan.model.Batiment;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class BatimentMapper implements IMapper<Batiment, BatimentDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public BatimentDTO map(Batiment batiment, BatimentDTO batimentDTO) {
       return this.getModelMapper().map(batiment, batimentDTO.getClass());
    }
}
