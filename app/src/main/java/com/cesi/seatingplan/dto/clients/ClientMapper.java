package com.cesi.seatingplan.dto.clients;

import com.cesi.seatingplan.dto.IMapper;
import com.cesi.seatingplan.model.Client;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ClientMapper implements IMapper<Client, ClientDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public ClientDTO map(Client client, ClientDTO clientDTO) {
       return this.getModelMapper().map(client, clientDTO.getClass());
    }
}
