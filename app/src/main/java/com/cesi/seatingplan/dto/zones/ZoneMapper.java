package com.cesi.seatingplan.dto.zones;

import com.cesi.seatingplan.dto.IMapper;
import com.cesi.seatingplan.model.Zone;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ZoneMapper implements IMapper<Zone, ZoneDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public ZoneDTO map(Zone zone, ZoneDTO zoneDTO) {
       return this.getModelMapper().map(zone, zoneDTO.getClass());
    }
}
