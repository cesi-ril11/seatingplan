package com.cesi.seatingplan.dto.administrateurs;

import com.cesi.seatingplan.dto.IMapper;
import com.cesi.seatingplan.model.Administrateur;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class AdministrateurMapper implements IMapper<Administrateur, AdministrateurDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public AdministrateurDTO map(Administrateur admin, AdministrateurDTO adminDTO) {
       return this.getModelMapper().map(admin, adminDTO.getClass());
    }
}
