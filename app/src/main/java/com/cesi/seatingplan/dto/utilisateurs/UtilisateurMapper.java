package com.cesi.seatingplan.dto.utilisateurs;

import com.cesi.seatingplan.dto.IMapper;
import com.cesi.seatingplan.model.Utilisateur;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UtilisateurMapper implements IMapper<Utilisateur, UtilisateurDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public UtilisateurDTO map(Utilisateur utilisateur, UtilisateurDTO utilisateurDTO) {
       return this.getModelMapper().map(utilisateur, utilisateurDTO.getClass());
    }
}
