package com.cesi.seatingplan.dto.bureaux;

import com.cesi.seatingplan.dto.IMapper;
import com.cesi.seatingplan.model.Bureau;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class BureauMapper implements IMapper<Bureau, BureauDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public BureauDTO map(Bureau bureau, BureauDTO bureauDTO) {
       return this.getModelMapper().map(bureau, bureauDTO.getClass());
    }
}
