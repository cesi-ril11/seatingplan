package com.cesi.seatingplan.dto.batiments;

public class BatimentDTO {
    private Long id;
    private String name;


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
