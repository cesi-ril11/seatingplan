package com.cesi.seatingplan.dto.versionsplans;

import com.cesi.seatingplan.dto.IMapper;
import com.cesi.seatingplan.model.VersionsPlan;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class VersionsPlanMapper implements IMapper<VersionsPlan, VersionsPlanDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public VersionsPlanDTO map(VersionsPlan version, VersionsPlanDTO versionDTO) {
       return this.getModelMapper().map(version, versionDTO.getClass());
    }
}
