package com.cesi.seatingplan.dto.administrateurs;

import java.util.Date;

public class AdministrateurDTO {
    private Date date_arrivee;
    private Date date_depart;


    public Date getDateArrivee() {
        return date_arrivee;
    }

    public void setDateArrivee(Date date_arrivee) {
        this.date_arrivee = date_arrivee;
    }

    public Date getDateDepart() {
        return date_depart;
    }

    public void setDateDepart(Date date_depart) {
        this.date_depart = date_depart;
    }
}
