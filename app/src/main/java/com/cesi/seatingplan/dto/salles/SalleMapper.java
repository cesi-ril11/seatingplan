package com.cesi.seatingplan.dto.salles;

import com.cesi.seatingplan.dto.IMapper;
import com.cesi.seatingplan.model.Salle;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SalleMapper implements IMapper<Salle, SalleDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public SalleDTO map(Salle salle, SalleDTO salleDTO) {
       return this.getModelMapper().map(salle, salleDTO.getClass());
    }
}
