package com.cesi.seatingplan.dto.roles;

import com.cesi.seatingplan.dto.IMapper;
import com.cesi.seatingplan.model.Role;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper implements IMapper<Role, RoleDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public RoleDTO map(Role role, RoleDTO roleDTO) {
       return this.getModelMapper().map(role, roleDTO.getClass());
    }
}
