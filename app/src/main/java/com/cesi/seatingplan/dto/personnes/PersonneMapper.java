package com.cesi.seatingplan.dto.personnes;

import com.cesi.seatingplan.dto.IMapper;
import com.cesi.seatingplan.model.Personne;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PersonneMapper implements IMapper<Personne, PersonneDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public PersonneDTO map(Personne personne, PersonneDTO personneDTO) {
       return this.getModelMapper().map(personne, personneDTO.getClass());
    }
}
