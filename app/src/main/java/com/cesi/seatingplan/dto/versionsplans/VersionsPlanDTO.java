package com.cesi.seatingplan.dto.versionsplans;

import java.util.Date;

public class VersionsPlanDTO {
    private Long id;
    private Date date;
    private String num_version;

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getVersion() {
        return num_version;
    }
}
