package com.cesi.seatingplan.dto.clients;

import com.cesi.seatingplan.dto.personnes.PersonneDTO;

public class ClientDTO extends PersonneDTO{
    private String login;
    private String password;


    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
