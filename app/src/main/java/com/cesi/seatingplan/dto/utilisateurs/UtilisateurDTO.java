package com.cesi.seatingplan.dto.utilisateurs;

import java.util.Date;

public class UtilisateurDTO {
    private Date date_arrivee;
    private Date date_depart;


    public Date getDateArrivee() {
        return date_arrivee;
    }

    public Date getDateDepart() {
        return date_depart;
    }

    public void setDateArrivee(Date date) {
        this.date_arrivee = date;
    }

    public void setDateDepart(Date date) {
        this.date_depart = date;
    }
}
