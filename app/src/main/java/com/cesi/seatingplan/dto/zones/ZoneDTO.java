package com.cesi.seatingplan.dto.zones;

public class ZoneDTO {
    private Long id;
    private String libelle;

    public Long getId() {
        return id;
    }


    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
