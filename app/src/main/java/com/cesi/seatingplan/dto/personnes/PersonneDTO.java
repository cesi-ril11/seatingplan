package com.cesi.seatingplan.dto.personnes;

public class PersonneDTO {
    private Long id;
    private String first_name;
    private String last_name;


    public Long getId() {
        return id;
    }

    public String getFullName() {
        return first_name + ' ' + last_name;
    }

    public void setFirstName(String firstname) {
        this.first_name = firstname;
    }

    public void setLastName(String lastname) {
        this.last_name = lastname;
    }
}
