package com.cesi.seatingplan.dto.plans;

public class PlanDTO {
    private Long id;
    private String libelle;
    private String url;

    public Long getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getUrl() {
        return url;
    }
}
