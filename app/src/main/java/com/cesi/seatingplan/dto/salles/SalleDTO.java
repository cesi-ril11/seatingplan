package com.cesi.seatingplan.dto.salles;

public class SalleDTO {
    private Long id;
    private String libelle;
    private Long capacite;

    public Long getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Long getCapacite() {
        return capacite;
    }
}
