package com.cesi.seatingplan.service.batiment;

import com.cesi.seatingplan.dto.batiments.BatimentDTO;
import com.cesi.seatingplan.dto.batiments.BatimentMapper;
import com.cesi.seatingplan.exceptions.BatimentNotFoundException;
import com.cesi.seatingplan.model.Batiment;
import com.cesi.seatingplan.repository.BatimentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class BatimentService implements IBatimentService {
    @Autowired
    private BatimentRepository batimentRepository;

    private BatimentMapper batimentMapper = new BatimentMapper();

    @Override
    public BatimentDTO getById(Long id) throws BatimentNotFoundException {
        Optional<Batiment> found = batimentRepository.findById(id);
        if (found.isPresent()) {
            return batimentMapper.map(found.get(), new BatimentDTO());
        } else {
            throw new BatimentNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    @Override
    public BatimentDTO create(@Valid Batiment batiment) {
        return batimentMapper.map(batimentRepository.save(batiment), new BatimentDTO());
    }
}
