package com.cesi.seatingplan.service.versionsPlan;

import com.cesi.seatingplan.dto.versionsplans.VersionsPlanDTO;
import com.cesi.seatingplan.exceptions.PlanNotFoundException;
import com.cesi.seatingplan.exceptions.VersionPlanNotFoundException;
import com.cesi.seatingplan.model.VersionsPlan;

import javax.validation.Valid;


public interface IVersionPlanService {
    public VersionsPlanDTO getById(Long id) throws VersionPlanNotFoundException;
    public VersionsPlanDTO create(@Valid VersionsPlan versionPlan);
}
