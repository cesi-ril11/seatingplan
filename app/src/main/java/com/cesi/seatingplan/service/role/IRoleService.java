package com.cesi.seatingplan.service.role;

import com.cesi.seatingplan.dto.roles.RoleDTO;
import com.cesi.seatingplan.exceptions.RoleNotFoundException;
import com.cesi.seatingplan.model.Role;

import javax.validation.Valid;


public interface IRoleService {
    public RoleDTO getById(Long id) throws RoleNotFoundException;
    public RoleDTO create(@Valid Role role);
}
