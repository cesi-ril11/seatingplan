package com.cesi.seatingplan.service.client;

import com.cesi.seatingplan.dto.clients.ClientDTO;
import com.cesi.seatingplan.exceptions.ClientNotFoundException;
import com.cesi.seatingplan.model.Client;
import com.cesi.seatingplan.service.personne.IPersonneService;

import javax.validation.Valid;


public interface IClientService {
    public ClientDTO getById(Long id) throws ClientNotFoundException;
    public ClientDTO create(@Valid Client client);
}
