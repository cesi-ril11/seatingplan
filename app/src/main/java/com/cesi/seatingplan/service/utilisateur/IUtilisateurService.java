package com.cesi.seatingplan.service.utilisateur;

import com.cesi.seatingplan.dto.utilisateurs.UtilisateurDTO;
import com.cesi.seatingplan.exceptions.UtilisateurNotFoundException;
import com.cesi.seatingplan.model.Utilisateur;


import javax.validation.Valid;


public interface IUtilisateurService {
    public UtilisateurDTO getById(Long id) throws UtilisateurNotFoundException;
    public UtilisateurDTO create(@Valid Utilisateur utilisateur);
}
