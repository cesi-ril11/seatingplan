package com.cesi.seatingplan.service.plan;

import com.cesi.seatingplan.dto.plans.PlanDTO;
import com.cesi.seatingplan.exceptions.PlanNotFoundException;
import com.cesi.seatingplan.model.Plan;

import javax.validation.Valid;


public interface IPlanService {
    public PlanDTO getById(Long id) throws PlanNotFoundException;
    public PlanDTO create(@Valid Plan plan);
}
