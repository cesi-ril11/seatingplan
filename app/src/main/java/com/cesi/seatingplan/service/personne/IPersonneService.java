package com.cesi.seatingplan.service.personne;

import com.cesi.seatingplan.dto.administrateurs.AdministrateurDTO;
import com.cesi.seatingplan.dto.clients.ClientDTO;
import com.cesi.seatingplan.dto.personnes.PersonneDTO;
import com.cesi.seatingplan.dto.utilisateurs.UtilisateurDTO;
import com.cesi.seatingplan.exceptions.PersonneNotFoundException;
import com.cesi.seatingplan.model.Administrateur;
import com.cesi.seatingplan.model.Utilisateur;

import ch.qos.logback.core.net.server.Client;

import javax.validation.Valid;


public interface IPersonneService {
    abstract PersonneDTO getPersonneById(Long id) throws PersonneNotFoundException;
    abstract ClientDTO getClientById(Long id) throws PersonneNotFoundException;
    abstract ClientDTO create(@Valid Client client);
}
