package com.cesi.seatingplan.service.client;

import com.cesi.seatingplan.dto.clients.ClientDTO;
import com.cesi.seatingplan.dto.clients.ClientMapper;
import com.cesi.seatingplan.exceptions.ClientNotFoundException;
import com.cesi.seatingplan.model.Client;
import com.cesi.seatingplan.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class ClientService implements IClientService {
    @Autowired
    private ClientRepository clientRepository;
    private ClientMapper clientMapper = new ClientMapper();

    @Override
    public ClientDTO getById(Long id) throws ClientNotFoundException {
        Optional<Client> found = clientRepository.findById(id);
        if (found.isPresent()) {
            return clientMapper.map(found.get(), new ClientDTO());
        } else {
            throw new ClientNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    public ClientDTO create(@Valid Client client) {
        return clientMapper.map(clientRepository.save(client), new ClientDTO());
    }
}
