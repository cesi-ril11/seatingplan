package com.cesi.seatingplan.service.salle;

import com.cesi.seatingplan.dto.salles.SalleDTO;
import com.cesi.seatingplan.exceptions.PlanNotFoundException;
import com.cesi.seatingplan.exceptions.SalleNotFoundException;
import com.cesi.seatingplan.model.Salle;

import javax.validation.Valid;


public interface ISalleService {
    public SalleDTO getById(Long id) throws SalleNotFoundException;
    public SalleDTO create(@Valid Salle salle);
}
