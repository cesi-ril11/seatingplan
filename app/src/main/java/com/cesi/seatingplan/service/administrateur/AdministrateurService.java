package com.cesi.seatingplan.service.administrateur;

import com.cesi.seatingplan.dto.administrateurs.AdministrateurDTO;
import com.cesi.seatingplan.dto.administrateurs.AdministrateurMapper;
import com.cesi.seatingplan.exceptions.AdministrateurNotFoundException;
import com.cesi.seatingplan.model.Administrateur;
import com.cesi.seatingplan.repository.AdministrateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class AdministrateurService implements IAdministrateurService {
    @Autowired
    private AdministrateurRepository administrateurRepository;
    private AdministrateurMapper administrateurMapper = new AdministrateurMapper();

    @Override
    public AdministrateurDTO getById(Long id) throws AdministrateurNotFoundException {
        Optional<Administrateur> found = administrateurRepository.findById(id);
        if (found.isPresent()) {
            return administrateurMapper.map(found.get(), new AdministrateurDTO());
        } else {
            throw new AdministrateurNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    public AdministrateurDTO create(@Valid Administrateur administrateur) {
        return administrateurMapper.map(administrateurRepository.save(administrateur), new AdministrateurDTO());
    }
}
