package com.cesi.seatingplan.service.batiment;

import com.cesi.seatingplan.dto.batiments.BatimentDTO;
import com.cesi.seatingplan.exceptions.BatimentNotFoundException;
import com.cesi.seatingplan.model.Batiment;

import javax.validation.Valid;


public interface IBatimentService {
    public BatimentDTO getById(Long id) throws BatimentNotFoundException;
    public BatimentDTO create(@Valid Batiment batiment);
}
