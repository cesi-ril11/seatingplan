package com.cesi.seatingplan.service.zone;

import com.cesi.seatingplan.dto.zones.ZoneDTO;
import com.cesi.seatingplan.dto.zones.ZoneMapper;
import com.cesi.seatingplan.exceptions.ZoneNotFoundException;
import com.cesi.seatingplan.model.Zone;
import com.cesi.seatingplan.repository.ZoneRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class ZoneService implements IZoneService {
    @Autowired
    private ZoneRepository zoneRepository;


    private ZoneMapper zoneMapper = new ZoneMapper();

    @Override
    public ZoneDTO getById(Long id) throws ZoneNotFoundException {
        Optional<Zone> found = zoneRepository.findById(id);
        if (found.isPresent()) {
            return zoneMapper.map(found.get(), new ZoneDTO());
        } else {
            throw new ZoneNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    @Override
    public ZoneDTO create(@Valid Zone zone) {
        return zoneMapper.map(zoneRepository.save(zone), new ZoneDTO());
    }
}
