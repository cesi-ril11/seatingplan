package com.cesi.seatingplan.service.materiel;

import com.cesi.seatingplan.dto.materiels.MaterielDTO;
import com.cesi.seatingplan.dto.materiels.MaterielMapper;
import com.cesi.seatingplan.exceptions.MaterielNotFoundException;
import com.cesi.seatingplan.model.Materiel;
import com.cesi.seatingplan.repository.MaterielRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class MaterielService implements IMaterielService {
    @Autowired
    private MaterielRepository materielRepository;

    private MaterielMapper materielMapper = new MaterielMapper();

    @Override
    public MaterielDTO getById(Long id) throws MaterielNotFoundException {
        Optional<Materiel> found = materielRepository.findById(id);
        if (found.isPresent()) {
            return materielMapper.map(found.get(), new MaterielDTO());
        } else {
            throw new MaterielNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    @Override
    public MaterielDTO create(@Valid Materiel materiel) {
        return materielMapper.map(materielRepository.save(materiel), new MaterielDTO());
    }
}
