package com.cesi.seatingplan.service.personne;

import com.cesi.seatingplan.dto.clients.ClientDTO;
import com.cesi.seatingplan.dto.personnes.PersonneDTO;
import com.cesi.seatingplan.dto.personnes.PersonneMapper;
import com.cesi.seatingplan.exceptions.PersonneNotFoundException;
import com.cesi.seatingplan.model.Client;
import com.cesi.seatingplan.model.Personne;
import com.cesi.seatingplan.repository.PersonneRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;

@Service
@Validated
public abstract class PersonneService implements IPersonneService {
    @Autowired
    private PersonneRepository personneRepository;

    private PersonneMapper personneMapper = new PersonneMapper();

    public abstract PersonneDTO getPersonneById(Long id) throws PersonneNotFoundException;
    public abstract ClientDTO getClientById(Long id) throws PersonneNotFoundException;

    @Validated(OnCreate.class)
    public abstract PersonneDTO create(@Valid Personne personne);
}
