package com.cesi.seatingplan.service.salle;

import com.cesi.seatingplan.dto.salles.SalleDTO;
import com.cesi.seatingplan.dto.salles.SalleMapper;
import com.cesi.seatingplan.exceptions.PlanNotFoundException;
import com.cesi.seatingplan.exceptions.SalleNotFoundException;
import com.cesi.seatingplan.model.Salle;
import com.cesi.seatingplan.repository.PlanRepository;
import com.cesi.seatingplan.repository.SalleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class SalleService implements ISalleService {
    @Autowired
    private SalleRepository salleRepository;


    private SalleMapper salleMapper = new SalleMapper();

    @Override
    public SalleDTO getById(Long id) throws SalleNotFoundException {
        Optional<Salle> found = salleRepository.findById(id);
        if (found.isPresent()) {
            return salleMapper.map(found.get(), new SalleDTO());
        } else {
            throw new SalleNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    @Override
    public SalleDTO create(@Valid Salle salle) {
        return salleMapper.map(salleRepository.save(salle), new SalleDTO());
    }
}
