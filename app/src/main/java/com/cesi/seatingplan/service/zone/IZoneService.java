package com.cesi.seatingplan.service.zone;

import com.cesi.seatingplan.dto.zones.ZoneDTO;
import com.cesi.seatingplan.exceptions.PlanNotFoundException;
import com.cesi.seatingplan.exceptions.ZoneNotFoundException;
import com.cesi.seatingplan.model.Zone;

import javax.validation.Valid;

public interface IZoneService {
    public ZoneDTO getById(Long id) throws ZoneNotFoundException;
    public ZoneDTO create(@Valid Zone zone);
}
