package com.cesi.seatingplan.service.role;

import com.cesi.seatingplan.dto.roles.RoleDTO;
import com.cesi.seatingplan.dto.roles.RoleMapper;
import com.cesi.seatingplan.exceptions.PlanNotFoundException;
import com.cesi.seatingplan.exceptions.RoleNotFoundException;
import com.cesi.seatingplan.model.Role;
import com.cesi.seatingplan.repository.PlanRepository;
import com.cesi.seatingplan.repository.RoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class RoleService implements IRoleService {
    @Autowired
    private RoleRepository roleRepository;


    private RoleMapper roleMapper = new RoleMapper();

    @Override
    public RoleDTO getById(Long id) throws RoleNotFoundException {
        Optional<Role> found = roleRepository.findById(id);
        if (found.isPresent()) {
            return roleMapper.map(found.get(), new RoleDTO());
        } else {
            throw new RoleNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    @Override
    public RoleDTO create(@Valid Role role) {
        return roleMapper.map(roleRepository.save(role), new RoleDTO());
    }
}
