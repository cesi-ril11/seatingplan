package com.cesi.seatingplan.service.administrateur;

import com.cesi.seatingplan.dto.administrateurs.AdministrateurDTO;
import com.cesi.seatingplan.exceptions.AdministrateurNotFoundException;
import com.cesi.seatingplan.model.Administrateur;

import javax.validation.Valid;


public interface IAdministrateurService {
    public AdministrateurDTO getById(Long id) throws AdministrateurNotFoundException;
    public AdministrateurDTO create(@Valid Administrateur administrateur);
}
