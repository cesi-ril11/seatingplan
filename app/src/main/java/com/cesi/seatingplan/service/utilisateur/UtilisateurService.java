package com.cesi.seatingplan.service.utilisateur;

import com.cesi.seatingplan.dto.utilisateurs.UtilisateurDTO;
import com.cesi.seatingplan.dto.utilisateurs.UtilisateurMapper;
import com.cesi.seatingplan.exceptions.UtilisateurNotFoundException;
import com.cesi.seatingplan.model.Utilisateur;
import com.cesi.seatingplan.repository.UtilisateurRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class UtilisateurService implements IUtilisateurService {
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    private UtilisateurMapper utilisateurMapper = new UtilisateurMapper();

    @Override
    public UtilisateurDTO getById(Long id) throws UtilisateurNotFoundException {
        Optional<Utilisateur> found = utilisateurRepository.findById(id);
        if (found.isPresent()) {
            return utilisateurMapper.map(found.get(), new UtilisateurDTO());
        } else {
            throw new UtilisateurNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    public UtilisateurDTO create(@Valid Utilisateur utilisateur) {
        return utilisateurMapper.map(utilisateurRepository.save(utilisateur), new UtilisateurDTO());
    }
}
