package com.cesi.seatingplan.service.bureau;

import com.cesi.seatingplan.dto.bureaux.BureauDTO;
import com.cesi.seatingplan.exceptions.BureauNotFoundException;
import com.cesi.seatingplan.model.Bureau;

import javax.validation.Valid;


public interface IBureauService {
    public BureauDTO getById(Long id) throws BureauNotFoundException;
    public BureauDTO create(@Valid Bureau bureau);
}
