package com.cesi.seatingplan.service.versionsPlan;

import com.cesi.seatingplan.dto.versionsplans.VersionsPlanDTO;
import com.cesi.seatingplan.dto.versionsplans.VersionsPlanMapper;
import com.cesi.seatingplan.exceptions.PlanNotFoundException;
import com.cesi.seatingplan.exceptions.VersionPlanNotFoundException;
import com.cesi.seatingplan.model.VersionsPlan;
import com.cesi.seatingplan.repository.PlanRepository;
import com.cesi.seatingplan.repository.VersionsPlanRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class VersionPlanService implements IVersionPlanService {
    @Autowired
    private VersionsPlanRepository versionsPlanRepository;


    private VersionsPlanMapper versionsPlanMapper = new VersionsPlanMapper();

    @Override
    public VersionsPlanDTO getById(Long id) throws VersionPlanNotFoundException {
        Optional<VersionsPlan> found = versionsPlanRepository.findById(id);
        if (found.isPresent()) {
            return versionsPlanMapper.map(found.get(), new VersionsPlanDTO());
        } else {
            throw new VersionPlanNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    @Override
    public VersionsPlanDTO create(@Valid VersionsPlan versionPlan) {
        return versionsPlanMapper.map(versionsPlanRepository.save(versionPlan), new VersionsPlanDTO());
    }
}
