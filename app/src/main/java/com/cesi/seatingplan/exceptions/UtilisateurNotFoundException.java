package com.cesi.seatingplan.exceptions;

public class UtilisateurNotFoundException extends Exception {
    public UtilisateurNotFoundException(Long id) {
        super("La personne avec l'id " + id + " n'existe pas");
    }
}
