package com.cesi.seatingplan.exceptions;

public class ClientNotFoundException extends PersonneNotFoundException {
    public ClientNotFoundException(Long id) {
        super("Le client avec l'id " + id + " n'existe pas");
    }
}
