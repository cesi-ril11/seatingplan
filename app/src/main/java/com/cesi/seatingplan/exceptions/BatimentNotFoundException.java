package com.cesi.seatingplan.exceptions;

public class BatimentNotFoundException extends Exception {
    public BatimentNotFoundException(Long id) {
        super("Le batiment avec l'id " + id + " n'existe pas");
    }
}
