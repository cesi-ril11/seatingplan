package com.cesi.seatingplan.exceptions;

public class ZoneNotFoundException extends Exception {
    public ZoneNotFoundException(Long id) {
        super("La zone avec l'id " + id + " n'existe pas");
    }
}
