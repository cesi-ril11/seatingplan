package com.cesi.seatingplan.exceptions;

public class MaterielNotFoundException extends Exception {
    public MaterielNotFoundException(Long id) {
        super("Le materiel avec l'id " + id + " n'existe pas");
    }
}
