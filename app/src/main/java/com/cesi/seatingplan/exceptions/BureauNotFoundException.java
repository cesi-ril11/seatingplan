package com.cesi.seatingplan.exceptions;

public class BureauNotFoundException extends Exception {
    public BureauNotFoundException(Long id) {
        super("Le bureau avec l'id " + id + " n'existe pas");
    }
}
