package com.cesi.seatingplan.exceptions;

public class AdministrateurNotFoundException extends Exception {
    public AdministrateurNotFoundException(Long id) {
        super("La personne avec l'id " + id + " n'existe pas");
    }
}
