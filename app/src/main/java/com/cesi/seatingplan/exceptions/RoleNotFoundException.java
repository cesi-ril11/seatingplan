package com.cesi.seatingplan.exceptions;

public class RoleNotFoundException extends Exception {
    public RoleNotFoundException(Long id) {
        super("Le role avec l'id " + id + " n'existe pas");
    }
}
