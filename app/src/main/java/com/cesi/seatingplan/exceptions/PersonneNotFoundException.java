package com.cesi.seatingplan.exceptions;

public class PersonneNotFoundException extends Exception {
    public PersonneNotFoundException(String string) {
        super("La personne avec l'id " + string + " n'existe pas");
    }
}
