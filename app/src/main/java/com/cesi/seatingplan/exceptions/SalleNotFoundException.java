package com.cesi.seatingplan.exceptions;

public class SalleNotFoundException extends Exception {
    public SalleNotFoundException(Long id) {
        super("La salle avec l'id " + id + " n'existe pas");
    }
}
