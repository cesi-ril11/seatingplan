package com.cesi.seatingplan.exceptions;

public class VersionPlanNotFoundException extends Exception {
    public VersionPlanNotFoundException(Long id) {
        super("La version du plan avec l'id " + id + " n'existe pas");
    }
}
