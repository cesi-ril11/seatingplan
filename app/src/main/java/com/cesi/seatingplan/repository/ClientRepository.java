package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.Client;

import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {
    
}