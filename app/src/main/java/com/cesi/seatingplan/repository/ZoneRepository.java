package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.Zone;
import org.springframework.data.repository.CrudRepository;

public interface ZoneRepository extends CrudRepository<Zone, Long> {
    
}