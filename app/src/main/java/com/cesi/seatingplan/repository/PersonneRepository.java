package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.Client;
import com.cesi.seatingplan.model.Personne;
import org.springframework.data.repository.CrudRepository;

public interface PersonneRepository extends CrudRepository<Personne, Long> {
    
}