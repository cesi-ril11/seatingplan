package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.VersionsPlan;
import org.springframework.data.repository.CrudRepository;

public interface VersionsPlanRepository extends CrudRepository<VersionsPlan, Long> {
    
}