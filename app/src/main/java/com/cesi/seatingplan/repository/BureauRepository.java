package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.Bureau;
import org.springframework.data.repository.CrudRepository;

public interface BureauRepository extends CrudRepository<Bureau, Long> {
    
}