package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
    
}