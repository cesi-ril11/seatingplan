package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.Salle;
import org.springframework.data.repository.CrudRepository;

public interface SalleRepository extends CrudRepository<Salle, Long> {
    
}