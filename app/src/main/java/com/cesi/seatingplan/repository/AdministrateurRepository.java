package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.Administrateur;
import org.springframework.data.repository.CrudRepository;

public interface AdministrateurRepository extends CrudRepository<Administrateur, Long> {
    
}