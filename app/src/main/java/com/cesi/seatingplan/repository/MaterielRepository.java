package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.Materiel;
import org.springframework.data.repository.CrudRepository;

public interface MaterielRepository extends CrudRepository<Materiel, Long> {
    
}