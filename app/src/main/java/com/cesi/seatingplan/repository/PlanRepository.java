package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.Plan;
import org.springframework.data.repository.CrudRepository;

public interface PlanRepository extends CrudRepository<Plan, Long> {
    
}