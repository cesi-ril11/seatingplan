package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.Utilisateur;
import org.springframework.data.repository.CrudRepository;

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Long> {
    
}