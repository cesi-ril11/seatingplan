package com.cesi.seatingplan.controller;

import com.cesi.seatingplan.dto.bureaux.BureauDTO;
import com.cesi.seatingplan.dto.materiels.MaterielDTO;
import com.cesi.seatingplan.exceptions.MaterielNotFoundException;
import com.cesi.seatingplan.model.Bureau;
import com.cesi.seatingplan.model.Materiel;
import com.cesi.seatingplan.repository.MaterielRepository;
import com.cesi.seatingplan.service.materiel.IMaterielService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;


@RestController
@RequestMapping(path="api/1.0") 
public class MaterielController {

    @Autowired
    private IMaterielService materielService;

    @Autowired
    private MaterielRepository materielRepository;

    @RequestMapping("/materiel")
    public @ResponseBody
    Iterable<Materiel> all() {
        return materielRepository.findAll();
    }
    
    @PostMapping(path="/materiel")
    public @ResponseBody MaterielDTO create (@RequestBody @Valid Materiel materiel) {
        return materielService.create(materiel);
    }


}