package com.cesi.seatingplan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cesi.seatingplan.model.Role;
import com.cesi.seatingplan.repository.RoleRepository;
import com.cesi.seatingplan.service.role.IRoleService;

@RestController
@RequestMapping(path="api/1.0") 
public class RoleController {
	
    @Autowired
    private IRoleService roleService;

    @Autowired
    private RoleRepository roleRepository;

    @RequestMapping("/Role")
    public @ResponseBody
    Iterable<Role> all() {
        return roleRepository.findAll();
    }	

}
