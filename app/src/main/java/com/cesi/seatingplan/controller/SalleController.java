package com.cesi.seatingplan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cesi.seatingplan.model.Salle;
import com.cesi.seatingplan.repository.SalleRepository;
import com.cesi.seatingplan.service.salle.ISalleService;

@RestController
@RequestMapping(path="api/1.0") 
public class SalleController {

    @Autowired
    private ISalleService salleService;

    @Autowired
    private SalleRepository salleRepository;

    @RequestMapping("/Salle")
    public @ResponseBody
    Iterable<Salle> all() {
        return salleRepository.findAll();
    }	
	
}
