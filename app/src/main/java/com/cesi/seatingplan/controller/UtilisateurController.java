package com.cesi.seatingplan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cesi.seatingplan.model.Utilisateur;
import com.cesi.seatingplan.repository.UtilisateurRepository;
import com.cesi.seatingplan.service.utilisateur.IUtilisateurService;

@RestController
@RequestMapping(path="api/1.0") 
public class UtilisateurController {

    @Autowired
    private IUtilisateurService utilisateurService;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @RequestMapping("/Utilisateur")
    public @ResponseBody
    Iterable<Utilisateur> all() {
        return utilisateurRepository.findAll();
    }	
	
}
