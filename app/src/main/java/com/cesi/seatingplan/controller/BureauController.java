package com.cesi.seatingplan.controller;

import com.cesi.seatingplan.dto.bureaux.BureauDTO;
import com.cesi.seatingplan.exceptions.BureauNotFoundException;
import com.cesi.seatingplan.model.Bureau;
import com.cesi.seatingplan.repository.BureauRepository;
import com.cesi.seatingplan.service.bureau.IBureauService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import javax.validation.Valid;


@RestController
@RequestMapping(path="api/1.0")
public class BureauController {

    @Autowired
    private IBureauService bureauService;

    @Autowired
    private BureauRepository bureauRepository;

    @GetMapping("/bureaux")
    public @ResponseBody
    Iterable<Bureau> all() {
        return bureauRepository.findAll();
    }

    @PostMapping(path="/bureaux")
    public @ResponseBody BureauDTO create (@RequestBody @Valid Bureau bureau) {
        return bureauService.create(bureau);
    }

    // GET a single bureau
    // @GetMapping(path="/bureaux/{id}")
    // public Bureau getBureauById(@PathVariable(value ="id") Long bureauId){
    //     return bureauRepository.findById(bureauId)
    //             .orElseThrow(() -> new ResourceNotFoundException("Bureau","id", bureauId)) ;
    // }
    // @GetMapping(path="/bureau/delete/{id}")
    // public String deleteBureau(@PathVariable(value ="id") Long bureauId){
    //     Bureau bureau = bureauRepository.findById(bureauId)
    //             .orElseThrow(() -> new ResourceNotFoundException("Bureau","id", bureauId)) ;
    //     bureauRepository.delete(bureau);
    //     return "deleted";
    // }
}
