package com.cesi.seatingplan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cesi.seatingplan.model.Zone;
import com.cesi.seatingplan.repository.ZoneRepository;
import com.cesi.seatingplan.service.zone.*;

@RestController
@RequestMapping(path="api/1.0") 
public class ZoneController {
	
    @Autowired
    private IZoneService zoneService;

    @Autowired
    private ZoneRepository zoneRepository;

    @RequestMapping("/Zone")
    public @ResponseBody
    Iterable<Zone> all() {
        return zoneRepository.findAll();
    }

}
