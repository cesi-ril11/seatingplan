package com.cesi.seatingplan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cesi.seatingplan.model.Administrateur;
import com.cesi.seatingplan.repository.AdministrateurRepository;
import com.cesi.seatingplan.service.administrateur.IAdministrateurService;

@RestController
@RequestMapping(path="api/1.0") 
public class AdministrateurController {
    @Autowired
    private IAdministrateurService administrateurService;

    @Autowired
    private AdministrateurRepository administrateurRepository;

    @RequestMapping("/Administrateur")
    public @ResponseBody
    Iterable<Administrateur> all() {
        return administrateurRepository.findAll();
    }	

}
