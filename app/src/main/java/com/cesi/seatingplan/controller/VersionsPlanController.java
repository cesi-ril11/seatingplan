package com.cesi.seatingplan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cesi.seatingplan.model.VersionsPlan;
import com.cesi.seatingplan.repository.VersionsPlanRepository;
import com.cesi.seatingplan.service.versionsPlan.IVersionPlanService;

@RestController
@RequestMapping(path="api/1.0") 
public class VersionsPlanController {
	
    @Autowired
    private IVersionPlanService versionsplanService;

    @Autowired
    private VersionsPlanRepository versionsplanRepository;

    @RequestMapping("/VersionsPlan")
    public @ResponseBody
    Iterable<VersionsPlan> all() {
        return versionsplanRepository.findAll();
    }	

}
