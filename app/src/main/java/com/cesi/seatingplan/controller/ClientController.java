package com.cesi.seatingplan.controller;

import com.cesi.seatingplan.model.Client;
import com.cesi.seatingplan.repository.ClientRepository;
import com.cesi.seatingplan.service.client.IClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "api/1.0")
public class ClientController {
    @Autowired
    private IClientService clientService;

    @Autowired
    private ClientRepository clientRepository;

    @RequestMapping("/client")    public @ResponseBody
    Iterable<Client> all() {
        return clientRepository.findAll();
    }
    
    /*

    @RequestMapping("/client/{id}")
    public @ResponseBody
    ClientDTO show(@PathVariable Long id) {
        try {
            return clientService.getById(id);
        } catch (ClientNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Client not found at id " + id + "."
            );
        }
    }

    @PostMapping(path = "/client")
    public @ResponseBody
    ClientDTO create(@Valid @RequestBody Client cli) {
        return clientService.create(cli);
    }
    
    */
}
