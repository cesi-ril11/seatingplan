package com.cesi.seatingplan.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Batiment implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String name;

    public Batiment() {
    }

    public Batiment(String name) {
        this.name = name;
    }

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
}