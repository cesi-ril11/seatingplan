package com.cesi.seatingplan.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

@Entity
public class Bureau implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long bureau_id;

    @NotBlank
    private String num_bureau;
    // @NotNull
    // private Boolean privatif;

    @OneToMany(mappedBy="bureau")
    private List<Materiel> customers;

    
    public Bureau() {
    }

    public Bureau(String num_bureau, Boolean prive) {
        this.num_bureau = num_bureau;
        //this.privatif = prive;
    }

    public Long getId() { return this.bureau_id; }

    public String getNumBureau() { return this.num_bureau; }

    public void setId(Long id) { this.bureau_id = id; }

    public void setNumBureau(String num_bureau) { this.num_bureau = num_bureau; }
}
