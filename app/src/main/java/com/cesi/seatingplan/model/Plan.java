package com.cesi.seatingplan.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Plan implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String libelle;
    @NotBlank
    private String url;

    public Plan() {
    }

    public Plan(String libelle, String url) {
        this.libelle = libelle;
        this.url = url;
    }

	public Long getId() {
		return id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
    
}