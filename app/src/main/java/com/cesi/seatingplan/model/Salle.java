package com.cesi.seatingplan.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Salle implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String libelle;
    @NotNull
    private Long capacite;

    public Salle() {
    }

    public Salle(String libelle, Long capacite) {
        this.libelle = libelle;
        this.capacite = capacite;
    }

	public Long getId() {
		return id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Long getCapacite() {
		return capacite;
	}

	public void setCapacite(Long capacite) {
		this.capacite = capacite;
	}
    
}