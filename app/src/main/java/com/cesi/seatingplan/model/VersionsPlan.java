package com.cesi.seatingplan.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class VersionsPlan implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private Date date;
    @NotBlank
    private String num_version;

    public VersionsPlan() {
    }

    public VersionsPlan(Date date, String num_version) {
        this.date = date;
        this.num_version = num_version;
    }

	public Long getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNum_version() {
		return num_version;
	}

	public void setNum_version(String num_version) {
		this.num_version = num_version;
	}
    
}