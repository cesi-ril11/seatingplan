package com.cesi.seatingplan.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Administrateur extends Personne {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private Date date_arrivee;
	private Date date_depart;

    public Administrateur(){
        super();
    }

    public Administrateur(Long id, String first_name, String last_name,Date date_arrivee, Date date_depart) {
    	super(id, first_name, last_name);
        this.date_arrivee = date_arrivee;
        this.date_depart = date_depart;
    }
    public Date getDate_arrivee() {
		return date_arrivee;
	}

	public void setDate_arrivee(Date date_arrivee) {
		this.date_arrivee = date_arrivee;
	}

	public Date getDate_depart() {
		return date_depart;
	}

	public void setDate_depart(Date date_depart) {
		this.date_depart = date_depart;
	}
}