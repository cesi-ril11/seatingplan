package com.cesi.seatingplan.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

@Entity
public class Materiel implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String libelle;
    
    @ManyToOne
    @JoinColumn(name="bureau_id")
    private Bureau bureau;


    public Materiel() {
    }

    public Materiel(String libelle) {
        this.libelle = libelle;
    }

	public Long getId() {
		return id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
    
}