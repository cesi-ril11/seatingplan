package com.cesi.seatingplan.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Client extends Personne {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String login;
    @NotBlank
    private String password;

    public Client() {
        super();
    }

    public Client(Long id, String first_name, String last_name, String login, String password) {
        super(id, first_name, last_name);
        this.login = login;
        this.password = password;
    }
}